package ua.goit.javaee.model;

/**
 * @author <a href="mailto:info@olegorlov.com">Oleg Orlov</a>
 */
class Person extends BaseEntity {

  private String firstName;
  private String lastName;

  Person(Integer id, String firstName, String lastName) {
    super(id);
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

}
