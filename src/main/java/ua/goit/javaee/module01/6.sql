-- 6. Вычислить, среднюю ЗП программистов в проекте, который приносит наименьшую прибыль.

SELECT
  projects.name          AS project_name,
  projects.cost          AS project_cost,
  avg(developers.salary) AS av_salary
FROM projects
  INNER JOIN project_developers
    ON projects.id = project_developers.project_id
  INNER JOIN developers
    ON developers.id = project_developers.developer_id
GROUP BY project_name, project_cost
ORDER BY project_cost ASC
/*LIMIT 1*/;