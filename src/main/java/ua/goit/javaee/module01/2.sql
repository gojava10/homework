﻿-- 2. Найти самый дорогой проект (исходя из ЗП разработчиков).

SELECT
  name,
  sum(d.salary) AS salary_sum
FROM developers d
  INNER JOIN project_developers
    ON d.id = developer_id
  INNER JOIN projects
    ON projects.id = project_id
GROUP BY name
ORDER BY salary_sum DESC
LIMIT 1;
